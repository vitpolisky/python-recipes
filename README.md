This repository contains code accompanying the articles in my blog <a href='python-recipes.com'>Python Recipes</a>.

To run the examples in this repo, it's recommended to create a Python 3 virtual environment and, with the virtual 
environment activated, execute `pip install -r requirements.txt`. This will install all the dependencies you need 
to run the examples. For this project I've used Python 3.8, but I'd expect the code to run
with Python 3.6+.

For each blog post, there's a dedicated branch which is linked in the post.